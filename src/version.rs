use std::fmt::{Display, Formatter, Result};

pub struct Version {
    major: u32,
    minor: u32,
    patch: u32,
}

impl Version {
    pub fn get() -> Self {
        let major = env!("CARGO_PKG_VERSION_MAJOR").parse().unwrap();
        let minor = env!("CARGO_PKG_VERSION_MINOR").parse().unwrap();
        let patch = env!("CARGO_PKG_VERSION_PATCH").parse().unwrap();

        Version {
            major,
            minor,
            patch,
        }
    }
}

impl Display for Version {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{}.{}.{}", self.major, self.minor, self.patch)
    }
}
