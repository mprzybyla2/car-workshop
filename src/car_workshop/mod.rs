mod client_manager;
mod mechanic_manager;
mod request_manager;
mod utilities {
    pub mod id_generator;
}

pub use client_manager::ClientManager;
pub use mechanic_manager::MechanicManager;
pub use request_manager::RequestManager;
use utilities::id_generator::IdGenerator;

pub struct CarWorkshop {
    mechanic_manager: Box<MechanicManager>,
    request_manager: Box<RequestManager>,
    client_manager: Box<ClientManager>,
}

impl CarWorkshop {
    pub fn new(
        mechanic_manager: Box<MechanicManager>,
        request_manager: Box<RequestManager>,
        client_manager: Box<ClientManager>,
    ) -> Self {
        Self {
            mechanic_manager,
            request_manager,
            client_manager,
        }
    }
}

impl Drop for CarWorkshop {
    fn drop(&mut self) {
        println!("Car Workshop exit");
    }
}
