pub struct IdGenerator {
    id: u32,
}

impl IdGenerator {
    pub fn new() -> Self {
        IdGenerator { id: 0 }
    }

    pub fn get_new_id(&mut self) -> u32 {
        self.id += 1;
        self.id
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_new() {
        let generator = IdGenerator::new();
        assert_eq!(generator.id, 0);
    }

    #[test]
    fn test_get_new_id() {
        let mut generator = IdGenerator::new();
        generator.get_new_id();
        assert_eq!(generator.id, 1);

        generator.get_new_id();
        generator.get_new_id();
        generator.get_new_id();
        assert_eq!(generator.id, 4);
    }
}
