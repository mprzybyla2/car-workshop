use chrono::prelude::*;

pub type RequestId = u32;

pub enum RequestType {
    Overview,
    OilChange,
    TyresChange { buy_new: bool },
    Repair(String),
}

type ClientId = u32;

pub struct Request {
    pub id: RequestId,
    client_id: ClientId,
    request_type: RequestType,
    creation_time: DateTime<Utc>,
}

impl Request {
    pub fn new(id: RequestId, client_id: ClientId, request_type: RequestType) -> Self {
        Self {
            id,
            client_id,
            request_type,
            creation_time: Utc::now(),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_new() {
        let now = Utc::now();
        let request_id = 7;
        let client_id = 10;
        let request_type = RequestType::Overview;
        let request = Request::new(request_id, client_id, request_type);
        assert!(request.creation_time >= now);
    }
}
