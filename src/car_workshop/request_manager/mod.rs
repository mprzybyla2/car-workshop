mod request;

use super::IdGenerator;
use chrono::prelude::*;
use request::{Request, RequestId, RequestType};

type ClientId = u32;
type MechanicId = u32;

pub struct RequestManager {
    awaiting_requests: Vec<Request>,
    ongoing_requests: Vec<(Request, MechanicId)>,
    finished_requests: Vec<(Request, DateTime<Utc>)>,
    id_generator: IdGenerator,
}

impl RequestManager {
    pub fn new() -> Self {
        Self {
            awaiting_requests: Vec::new(),
            ongoing_requests: Vec::new(),
            finished_requests: Vec::new(),
            id_generator: IdGenerator::new(),
        }
    }

    pub fn add_new_request(
        &mut self,
        client_id: ClientId,
        request_type: RequestType,
        mechanic_id: Option<MechanicId>,
    ) -> RequestId {
        let id = self.id_generator.get_new_id();
        let request = Request::new(id, client_id, request_type);
        if let Some(mechanic_id) = mechanic_id {
            self.ongoing_requests.push((request, mechanic_id));
        } else {
            self.awaiting_requests.push(request);
        }
        id
    }

    pub fn finish_request(&mut self, id: RequestId) -> Option<MechanicId> {
        let mut mechanic_id = None;
        self.take_awaiting_request(id)
            .or(self.take_ongoing_request(id).map(|(request, mech_id)| {
                mechanic_id = Some(mech_id);
                request
            }))
            .and_then(|request| Some(self.finished_requests.push((request, Utc::now()))));
        mechanic_id
    }

    fn take_awaiting_request(&mut self, id: RequestId) -> Option<Request> {
        self.awaiting_requests
            .iter()
            .position(|request| request.id == id)
            .and_then(|index| Some(self.awaiting_requests.swap_remove(index)))
    }

    fn take_ongoing_request(&mut self, id: RequestId) -> Option<(Request, MechanicId)> {
        self.ongoing_requests
            .iter()
            .position(|(request, _)| request.id == id)
            .and_then(|index| Some(self.ongoing_requests.swap_remove(index)))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_new() {
        let manager = RequestManager::new();
        assert_eq!(manager.awaiting_requests.len(), 0);
        assert_eq!(manager.ongoing_requests.len(), 0);
        assert_eq!(manager.finished_requests.len(), 0);
    }

    #[test]
    fn test_add_new_request() {
        let mut manager = RequestManager::new();

        let client_id = 13;
        let request_type = RequestType::Repair(String::from("The battery does not charge."));
        let mechanic_id = None;
        manager.add_new_request(client_id, request_type, mechanic_id);
        assert_eq!(manager.awaiting_requests.len(), 1);

        let client_id = 145;
        let request_type = RequestType::TyresChange { buy_new: true };
        let mechanic_id = Some(18);
        manager.add_new_request(client_id, request_type, mechanic_id);
        assert_eq!(manager.ongoing_requests.len(), 1);
    }

    #[test]
    fn test_finish_request() {
        let mut manager = RequestManager::new();

        let request_id = 5;
        manager
            .awaiting_requests
            .push(Request::new(request_id, 10, RequestType::OilChange));

        let free_mechanic = manager.finish_request(request_id);
        assert_eq!(free_mechanic, None);
        assert_eq!(manager.awaiting_requests.len(), 0);
        assert_eq!(manager.finished_requests.len(), 1);

        let request_id = 10;
        let mechanic_id = 88;
        manager.ongoing_requests.push((
            Request::new(request_id, 20, RequestType::Overview),
            mechanic_id,
        ));

        let free_mechanic = manager.finish_request(request_id);
        assert_eq!(free_mechanic, Some(mechanic_id));
        assert_eq!(manager.ongoing_requests.len(), 0);
        assert_eq!(manager.finished_requests.len(), 2);
    }
}
