mod mechanic;

use super::IdGenerator;
use mechanic::{Mechanic, MechanicId};

type RequestId = u32;

pub struct MechanicManager {
    available_mechanics: Vec<Mechanic>,
    busy_mechanics: Vec<(Mechanic, RequestId)>,
    id_generator: IdGenerator,
}

impl MechanicManager {
    pub fn new() -> Self {
        Self {
            available_mechanics: Vec::new(),
            busy_mechanics: Vec::new(),
            id_generator: IdGenerator::new(),
        }
    }

    pub fn hire(&mut self, name: String) {
        self.available_mechanics.push(Mechanic {
            id: self.id_generator.get_new_id(),
            name,
        });
    }

    pub fn fire(&mut self, id: MechanicId) -> Option<RequestId> {
        self.available_mechanics
            .iter()
            .position(|mechanic| mechanic.id == id)
            .and_then(|index| {
                self.available_mechanics.swap_remove(index);
                None
            })
            .or_else(|| {
                self.busy_mechanics
                    .iter()
                    .position(|(mechanic, _)| mechanic.id == id)
                    .and_then(|index| {
                        let request_id = self.busy_mechanics[index].1;
                        self.busy_mechanics.swap_remove(index);
                        Some(request_id)
                    })
            })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_new() {
        let manager = MechanicManager::new();
        assert_eq!(manager.available_mechanics.len(), 0);
        assert_eq!(manager.busy_mechanics.len(), 0);
    }

    #[test]
    fn test_hire() {
        let mut manager = MechanicManager::new();

        manager.hire(String::from("John Doe"));
        assert_eq!(manager.available_mechanics.len(), 1);
        assert_eq!(manager.busy_mechanics.len(), 0);
    }

    #[test]
    fn test_fire() {
        let mut manager = MechanicManager::new();
        manager.available_mechanics.push(Mechanic {
            id: 17,
            name: String::from("Benny Hill"),
        });
        manager.busy_mechanics.push((
            Mechanic {
                id: 13,
                name: String::from("Max Payne"),
            },
            25,
        ));

        let request_id = manager.fire(121);
        assert_eq!(request_id, None);
        assert_eq!(manager.available_mechanics.len(), 1);
        assert_eq!(manager.busy_mechanics.len(), 1);

        let request_id = manager.fire(17);
        assert_eq!(request_id, None);
        assert_eq!(manager.available_mechanics.len(), 0);

        let request_id = manager.fire(13);
        assert_eq!(request_id, Some(25));
        assert_eq!(manager.busy_mechanics.len(), 0);
    }
}
