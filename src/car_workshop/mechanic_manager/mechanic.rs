pub type MechanicId = u32;

pub struct Mechanic {
    pub id: MechanicId,
    pub name: String,
}
