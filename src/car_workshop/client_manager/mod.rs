mod client;

use super::IdGenerator;
use client::{Client, ClientId};

pub struct ClientManager {
    known_clients: Vec<Client>,
    id_generator: IdGenerator,
}

impl ClientManager {
    pub fn new() -> Self {
        Self {
            known_clients: Vec::new(),
            id_generator: IdGenerator::new(),
        }
    }

    pub fn add_client(&mut self, name: String) -> ClientId {
        self.find_client(&name)
            .or_else(move || {
                let id = self.id_generator.get_new_id();
                self.known_clients.push(Client { id, name });
                Some(id)
            })
            .unwrap()
    }

    fn find_client(&self, name: &str) -> Option<ClientId> {
        self.known_clients
            .iter()
            .find(|client| client.name == name)
            .map(|client| client.id)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_new() {
        let manager = ClientManager::new();
        assert_eq!(manager.known_clients.len(), 0);
    }

    #[test]
    fn test_add_client() {
        let mut manager = ClientManager::new();
        manager.add_client(String::from("John Walsh"));
        assert_eq!(manager.known_clients.len(), 1);
    }

    #[test]
    fn test_find_client() {
        let mut manager = ClientManager::new();
        manager.known_clients.push(Client {
            id: 7,
            name: String::from("Xavier Hollander"),
        });
        let client_id = manager.find_client("Xavier Hollander");
        assert_eq!(client_id, Some(7));

        let client_id = manager.find_client("Zdzislaw Beksinski");
        assert_eq!(client_id, None);
    }
}
