pub type ClientId = u32;

pub struct Client {
    pub id: ClientId,
    pub name: String,
}
