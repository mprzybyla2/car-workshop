#![allow(dead_code)]

mod version;
use version::Version;

mod car_workshop;
use car_workshop::{CarWorkshop, ClientManager, MechanicManager, RequestManager};

fn main() {
    println!("Car Workshop ver. {}", Version::get());

    let mechanic_manager = Box::new(MechanicManager::new());
    let request_manager = Box::new(RequestManager::new());
    let client_manager = Box::new(ClientManager::new());

    let _car_workshop = CarWorkshop::new(mechanic_manager, request_manager, client_manager);
}
